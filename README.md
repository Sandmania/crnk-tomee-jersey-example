[![Master branch build status](https://gitlab.com/Sandmania/crnk-tomee-jersey-example/badges/master/build.svg)](https://gitlab.com/Sandmania/crnk-tomee-jersey-example/pipelines)
[![SonarCloud coverage](https://sonarcloud.io/api/project_badges/measure?project=sandmania.crnk%3Acrnk-tomee-boilerplate&metric=coverage)](https://sonarcloud.io/dashboard?id=sandmania.crnk%3Acrnk-tomee-boilerplate)
# Using Crnk with TomEE, Jersey and CDI

Simple example application that shows how to use [Crnk](http://www.crnk.io/) - A Java [JSON API](http://jsonapi.org/) implementation - 
with JAX-RS and CDI integration on top of TomEE. Instead of Apache CXF, it uses Jersey as its JAX-RS implementation. 

## Running the application

`mvn tomee:start`   

Then open [http://localhost:8080/tomee-jersey/projects](http://localhost:8080/tomee-jersey/projects) for instance.

## Tests

Integration tests are run against embedded TomEE to ensure functionality on the actual application server. 
Tests also leverage [PODAM](https://devopsfolks.github.io/podam/) for test data generation and 
Crnk-client for querying the endpoints.

Tests use JUnit 5.

Running tests: `mvn clean verify`
