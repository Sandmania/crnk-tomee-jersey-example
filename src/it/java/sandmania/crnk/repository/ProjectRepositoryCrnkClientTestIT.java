package sandmania.crnk.repository;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryV2;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sandmania.crnk.IntegrationTestBase;
import sandmania.crnk.resource.Project;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test that uses Crnk-client instead of JAX-RS client. See {@link ProjectRepositoryTestIT} for JAX-RS Client examples.
 */
public class ProjectRepositoryCrnkClientTestIT extends IntegrationTestBase {

    ResourceRepositoryV2<Project, Long> projectRepo = client.getRepositoryForType(Project.class);

    @Nested
    class WhenEmptyRepository {

        @Test
        void noProjectsPresent() {
            List<Project> foundProjects = projectRepo.findAll(new QuerySpec(Project.class));
            assertTrue(foundProjects.isEmpty());
        }

        @Nested
        @TestInstance(TestInstance.Lifecycle.PER_CLASS)
        class AfterCreateProject {

            Project createdProject;

            @BeforeAll
            void createNewProject() {
                Project project = factory.manufacturePojo(Project.class);
                createdProject = projectRepo.create(project);
                assertThat(createdProject.getId()).isNotNull();
                assertThat(createdProject.getName()).isEqualTo(project.getName());
            }

            @Test
            void oneProjectPresent() {

                List<Project> foundProjects = projectRepo.findAll(new QuerySpec(Project.class));

                assertFalse(foundProjects.isEmpty());
                assertThat(foundProjects.size()).isEqualTo(1);
                assertThat(foundProjects.get(0)).isEqualToComparingFieldByField(createdProject);
            }

            @Nested
            @TestInstance(TestInstance.Lifecycle.PER_CLASS)
            class AfterDeleteProject {

                @BeforeAll
                void deleteProject() {
                    projectRepo.delete(createdProject.getId());
                }

                @Test
                void noProjectsPresent() {
                    List<Project> foundProjects = projectRepo.findAll(new QuerySpec(Project.class));
                    assertTrue(foundProjects.isEmpty());
                }

            }
        }

    }

}