package sandmania.crnk.repository;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryV2;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sandmania.crnk.IntegrationTestBase;
import sandmania.crnk.resource.Project;
import sandmania.crnk.resource.Task;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TaskRepositoryCrnkClientTestIT extends IntegrationTestBase {

    ResourceRepositoryV2<Task, Long> taskRepo = client.getRepositoryForType(Task.class);

    @Nested
    class WhenEmptyRepository {

        @Test
        void noTasksPresent() {
            List<Task> foundTasks = taskRepo.findAll(new QuerySpec(Project.class));
            assertTrue(foundTasks.isEmpty());
        }

        @Nested
        @TestInstance(TestInstance.Lifecycle.PER_CLASS)
        class AfterCreateTask {

            Task createdTask;

            @BeforeAll
            void createNewTask() {
                Task task = factory.manufacturePojo(Task.class);
                createdTask = taskRepo.create(task);
                assertThat(createdTask.getId()).isNotNull();
                assertThat(createdTask.getName()).isEqualTo(task.getName());
            }

            @Test
            void oneTaskPresent() {
                List<Task> foundTasks = taskRepo.findAll(new QuerySpec(Task.class));

                assertFalse(foundTasks.isEmpty());
                assertThat(foundTasks.size()).isEqualTo(1);
                assertThat(foundTasks.get(0)).isEqualToComparingFieldByField(createdTask);
            }

            @Nested
            @TestInstance(TestInstance.Lifecycle.PER_CLASS)
            class AfterDeleteTask {

                @BeforeAll
                void deleteTask() {
                    taskRepo.delete(createdTask.getId());
                }

                @Test
                void noTasksPresent() {
                    List<Task> foundTasks = taskRepo.findAll(new QuerySpec(Task.class));
                    assertTrue(foundTasks.isEmpty());
                }

            }
        }

    }

}