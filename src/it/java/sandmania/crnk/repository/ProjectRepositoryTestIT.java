package sandmania.crnk.repository;

import io.crnk.client.CrnkClient;
import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryV2;
import io.crnk.rs.type.JsonApiMediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sandmania.crnk.resource.Project;

import javax.json.Json;
import javax.json.JsonReader;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.io.StringReader;
import java.util.List;

import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ProjectRepositoryTestIT {

    private static final Logger log = LogManager.getLogger(ProjectRepositoryTestIT.class);

    CrnkClient client = new CrnkClient("http://localhost:8080/tomee-jersey");
    ResourceRepositoryV2<Project, Long> projectRepo = client.getRepositoryForType(Project.class);

    @Nested
    class WhenEmptyRepository {

        @Test
        void noProjectsPresent() {
            Response response = get("/projects");

            assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

            String responseJsonString = response.readEntity(String.class);
            assertThatJson(responseJsonString).node("data").isArray().ofLength(0);
        }

        @Test
        void noProjectsPresentTestUsingCrnkCLient() {
            List<Project> allProjects = projectRepo.findAll(new QuerySpec(Project.class));
            assertThat(allProjects).isEmpty();
        }

        @Nested
        @TestInstance(TestInstance.Lifecycle.PER_CLASS)
        class AfterCreateProject {

            Long createdProjectId;

            @BeforeAll
            void createNewProject() {
                String payload = "{\n" +
                        "  \"data\" : {\n" +
                        "    \"type\" : \"projects\",\n" +
                        "    \"attributes\" : {\n" +
                        "      \"name\" : \"Project\",\n" +
                        "      \"startDate\" : \"2013-01-11\"\n" +
                        "    }\n" +
                        "  }\n" +
                        "}";

                Response response = post(payload, "/projects");

                String responseJsonString = response.readEntity(String.class);

                log.trace(responseJsonString);

                assertAll("project",
                        () -> assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode()),
                        () -> assertThatJson(responseJsonString).node("data").isPresent(),
                        () -> assertThatJson(responseJsonString).node("data.type").isStringEqualTo("projects"),
                        () -> assertThatJson(responseJsonString).node("data.id").isPresent()
                );

                JsonReader reader = Json.createReader(new StringReader(responseJsonString));
                createdProjectId = Long.valueOf(reader.readObject().getJsonObject("data").getString("id"));

            }

            @Test
            void oneProjectPresent() {
                Response response = get("/projects");

                String responseJsonString = response.readEntity(String.class);

                log.trace(responseJsonString);

                assertAll("found project",
                        () -> assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode()),
                        () -> assertThatJson(responseJsonString).node("data").isArray().ofLength(1),
                        () -> assertThatJson(responseJsonString).node("data[0].id").isStringEqualTo(createdProjectId.toString())
                );

            }

            @Nested
            @TestInstance(TestInstance.Lifecycle.PER_CLASS)
            class AfterDeleteProject {

                @BeforeAll
                void deleteProject() {
                    Response response = delete("/projects/" + createdProjectId);

                    assertAll("deleted project",
                            () -> assertThat(response.getStatus()).isEqualTo(Response.Status.NO_CONTENT.getStatusCode())
                    );
                }

                @Test
                void noProjectsPresent() {
                    Response response = get("/projects");

                    assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());

                    String responseJsonString = response.readEntity(String.class);
                    assertThatJson(responseJsonString).node("data").isArray().ofLength(0);
                }

            }
        }

    }

    @Test
    public void testSomething() {
        assertTrue(true);
    }


    private Response get(String path) {
        return ClientBuilder.newClient()
                .target("http://localhost:8080/tomee-jersey")
                .path(path)
                .request(JsonApiMediaType.APPLICATION_JSON_API)
                .get();
    }

    private Response post(String payload, String path) {
        return ClientBuilder.newClient()
                .target("http://localhost:8080/tomee-jersey")
                .path(path)
                .request(JsonApiMediaType.APPLICATION_JSON_API)
                .post(Entity.entity(payload, JsonApiMediaType.APPLICATION_JSON_API_TYPE));
    }

    private Response delete(String path) {
        return ClientBuilder.newClient()
                .target("http://localhost:8080/tomee-jersey")
                .path(path)
                .request(JsonApiMediaType.APPLICATION_JSON_API)
                .delete();
    }

}