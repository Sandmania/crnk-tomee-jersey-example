package sandmania.crnk;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.crnk.client.CrnkClient;
import io.crnk.core.resource.annotations.JsonApiId;
import io.crnk.core.resource.annotations.JsonApiRelation;
import uk.co.jemos.podam.api.DefaultClassInfoStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class IntegrationTestBase {

    protected static PodamFactory factory = new PodamFactoryImpl();
    protected static CrnkClient client = new CrnkClient("http://localhost:8080/tomee-jersey");

    public IntegrationTestBase() {
        client.getObjectMapper().registerModule(new JavaTimeModule());
        client.getObjectMapper().disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        factory.setClassStrategy(DefaultClassInfoStrategy.getInstance()
                .addExcludedAnnotation(JsonApiId.class)
                .addExcludedAnnotation(JsonApiRelation.class)
        );
    }

}
