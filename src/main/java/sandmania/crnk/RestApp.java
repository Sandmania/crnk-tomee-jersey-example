package sandmania.crnk;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.crnk.rs.CrnkFeature;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/")
public class RestApp extends ResourceConfig {

	private static final Logger log = LogManager.getLogger(RestApp.class);

	public RestApp() {
		log.traceEntry();
		
		try {
			packages("sandmania.crnk");
			CrnkFeature cf = new CrnkFeature();
			cf.getBoot().setObjectMapper(getObjectMapperForJsonApi());
			register(cf);
		} catch (Exception e) {
			log.error(e);
		}
		log.traceExit();
	}
	
    public static ObjectMapper getObjectMapperForJsonApi() {
        ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper;
    }

}
