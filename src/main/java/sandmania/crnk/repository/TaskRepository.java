package sandmania.crnk.repository;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryBase;
import io.crnk.core.resource.list.ResourceList;
import sandmania.crnk.resource.Task;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class TaskRepository extends ResourceRepositoryBase<Task, Long> {

    private static final AtomicLong ID = new AtomicLong(0L);
    private static final Map<Long, Task> REPOSITORY = new ConcurrentHashMap<>();

    public TaskRepository() {
        super(Task.class);
    }

    @Override
    public ResourceList<Task> findAll(QuerySpec querySpec) {
        return querySpec.apply(REPOSITORY.values());
    }

    @Override
    public <S extends Task> S save(S resource) {
        // As per jsonapi specification, client generated IDs are ok. Thus, this entity might already have an id.
        if(resource.getId() == null) {
            resource.setId(ID.incrementAndGet());
        }
        REPOSITORY.put(resource.getId(), resource);
        return resource;
    }

    @Override
    public <S extends Task> S create(S resource) {
        return save(resource);
    }

    @Override
    public void delete(Long id) {
        REPOSITORY.remove(id);
    }
}
