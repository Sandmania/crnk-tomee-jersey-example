package sandmania.crnk.repository;

import io.crnk.core.queryspec.QuerySpec;
import io.crnk.core.repository.ResourceRepositoryBase;
import io.crnk.core.resource.list.ResourceList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sandmania.crnk.resource.Project;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class ProjectRepository extends ResourceRepositoryBase<Project, Long> {

	private static final Logger log = LogManager.getLogger(ProjectRepository.class);
    private static final AtomicLong ID = new AtomicLong(0L);
    private static final Map<Long, Project> REPOSITORY = new ConcurrentHashMap<>();

    protected ProjectRepository() {
        super(Project.class);
    }

    @Override
	public Class<Project> getResourceClass() {
		return Project.class;
	}

	@Override
	public ResourceList<Project> findAll(QuerySpec querySpec) {
		return querySpec.apply(REPOSITORY.values());
	}


	@Override
	public void delete(Long id) {
	    REPOSITORY.remove(id);
	}

	@Override
    public <S extends Project> S create(S resource) {
	    // As per jsonapi specification, client generated IDs are ok. Thus, this resource might already have an id.
	    if(resource.getId() == null) {
            resource.setId(ID.incrementAndGet());
        }
        REPOSITORY.put(resource.getId(), resource);
		return resource;
	}

	@Override
    public <S extends Project> S save(S resource) {
		return create(resource);
	}
}
